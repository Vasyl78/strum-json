# frozen_string_literal: true

RSpec.describe Strum::Json::DeepKeysToSym do # rubocop: disable Metrics/BlockLength
  let(:payload) do
    {
      "key1" => [
        {
          "key2" => "value2",
          "key3" => [
            {
              "key4" => [
                {
                  "key5" => [
                    {
                      "key6" => "value6"
                    }
                  ],
                  "key7" => [
                    {
                      "key8" => "value8"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  end

  let(:output) do
    {
      key1: [
        {
          key2: "value2",
          key3: [
            {
              key4: [
                {
                  key5: [
                    {
                      key6: "value6"
                    }
                  ],
                  key7: [
                    { key8: "value8" }
                  ]
                }
              ]
            }
          ]
        }
      ]
    }
  end

  context "when payload contains array of hashes" do
    it "return symbolized keys" do
      described_class.call(payload) do |m|
        m.success { |result| expect(result).to eq(output) }
        m.failure { |error| raise "return to error block, unexpected result, #{error}" }
      end
    end
  end

  context "when payload contains simple hash" do
    let(:payload) do
      {
        "key1" => "value1",
        "key2" => "value2",
        "key3" => "value3"
      }
    end

    let(:output) do
      {
        key1: "value1",
        key2: "value2",
        key3: "value3"
      }
    end

    it "return symbolized keys" do
      described_class.call(payload) do |m|
        m.success { |result| expect(result).to eq(output) }
        m.failure { |error| raise "return to error block, unexpected result, #{error}" }
      end
    end
  end

  context "when payload contains array of hashes arrays" do # rubocop: disable Metrics/BlockLength
    let(:payload) do
      {
        "key1" => [
          [
            {
              "key2" => "value2"
            },
            {
              "key3" => "value3"
            }
          ]
        ]
      }
    end

    let(:output) do
      {
        key1: [
          [
            {
              key2: "value2"
            },
            {
              key3: "value3"
            }
          ]
        ]
      }
    end

    it "return symbolized keys" do
      described_class.call(payload) do |m|
        m.success { |result| expect(result).to eq(output) }
        m.failure { |error| raise "return to error block, unexpected result, #{error}" }
      end
    end
  end
end
